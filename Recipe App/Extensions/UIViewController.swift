//
//  UIViewController.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import UIKit


extension UIViewController {
    
    public func showMessagePrompt(title: String?, message: String!) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
