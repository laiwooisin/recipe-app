//
//  String.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import Foundation

extension String {
    var stringByDeletingLastPathComponent: String {
        get {
            return (self as NSString).deletingLastPathComponent
        }
    }
}
