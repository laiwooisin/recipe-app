//
//  ViewController.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 26/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var recipes: [Category : [Recipe]]! = [Category : [Recipe]]()
    private var categories = [Category]()
    private var selectedEditRecipe: Recipe!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadRecipes()
        if self.recipes.values.count <= 0 {
             self.recipes.removeAll()
             self.loadSamples()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRecipeDetails" {
            if let vc = segue.destination as? NewRecipeViewController {
                vc.editingRecipe = self.selectedEditRecipe
            }
        }
    }
    
    private func loadSamples() {
        let path = Bundle.main.path(forResource: "samples", ofType: "plist")!
        if  let dict = NSDictionary(contentsOfFile: path),
            let array = dict.object(forKey: "recipes") as? [[String : Any]] {
            
            var tempArr = [[String : Any]]()
            for aRecData in array {
                var aRecipe = Recipe()
                aRecipe.title = aRecData["name"] as? String
                aRecipe.ingredients = aRecData["ingredients"] as? String
                aRecipe.steps = aRecData["steps"] as? String
                
                if let bundlePath = Bundle.main.path(forResource: aRecData["imagePath"] as? String, ofType: "jpg"),
                    let image = UIImage(contentsOfFile: bundlePath) {
                    let encodedImg = image.jpegData(compressionQuality: 0.5)?.base64EncodedString()
                    aRecipe.imagePath = encodedImg
                }
                
                if let catDatas = aRecData["category"] as? [String : Any] {
                    let aCat = Category(name: catDatas["catName"] as? String ?? "")
                    aRecipe.category = aCat
                    
                    if self.categories.contains(aCat) {
                        self.recipes[aCat]?.append(aRecipe)
                    } else {
                        self.categories.append(aCat)
                        self.recipes[aCat] = [aRecipe]
                    }
                }
                
                let data: [String : Any] = [
                    "id" : aRecipe.id ?? "",
                    "name" : aRecipe.title ?? "",
                    "imagePath" : aRecipe.imagePath ?? "",
                    "ingredients" : aRecipe.ingredients ?? "",
                    "steps" : aRecipe.steps ?? "",
                    "category" : [
                        "catName" : aRecipe.category?.name
                    ]
                ]
                tempArr.append(data)
            }
            UserDefaults.standard.set(tempArr, forKey: "recipes")
            UserDefaults.standard.synchronize()
        }
        self.tableView.reloadData()
    }
    
    private func loadRecipes() {
        self.recipes.removeAll()
        self.categories.removeAll()
        
        if let datas = UserDefaults.standard.object(forKey: "recipes") as? [[String : Any]] {
            for aRecData in datas {
                var aRecipe = Recipe()
                aRecipe.id = aRecData["id"] as? String
                aRecipe.title = aRecData["name"] as? String
                aRecipe.imagePath = aRecData["imagePath"] as? String
                aRecipe.ingredients = aRecData["ingredients"] as? String
                aRecipe.steps = aRecData["steps"] as? String
                
                if let catDatas = aRecData["category"] as? [String : Any] {
                    let aCat = Category(name: catDatas["catName"] as? String ?? "")
                    aRecipe.category = aCat
                    
                    if self.categories.contains(aCat) {
                        self.recipes[aCat]?.append(aRecipe)
                    } else {
                        self.categories.append(aCat)
                        self.recipes[aCat] = [aRecipe]
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cat = self.categories[section]
        if let arr = self.recipes[cat] {
            return arr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        cell.lblTitle.text  = ""
        cell.vImage.image = nil
        cell.lblDesc.text = ""
        
        let cat = self.categories[indexPath.section]
        if let recipes = self.recipes[cat] {
            let aRecipe = recipes[indexPath.row]
            cell.lblTitle.text = aRecipe.title
            
            if let data = Data(base64Encoded: aRecipe.imagePath) {
                let image = UIImage(data: data)
                cell.vImage.image = image
            }
            cell.lblDesc.text = aRecipe.ingredients
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cat = self.categories[section]
        
        let headerView = UIView()
        headerView.backgroundColor = #colorLiteral(red: 0.2260365784, green: 0.3279403746, blue: 0.4710900187, alpha: 1)
        var constraints = [NSLayoutConstraint]()
        
        let lblCatName = UILabel()
        lblCatName.textColor = .white
        lblCatName.text = cat.name
        lblCatName.numberOfLines = 1
        lblCatName.sizeToFit()
        lblCatName.font = UIFont.systemFont(ofSize: 13.0)
        lblCatName.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        
        let lblMore = UILabel()
        lblMore.textColor = .white
        lblMore.text = ""
        lblMore.numberOfLines = 1
        lblMore.font = UIFont.systemFont(ofSize: 13.0)
        lblMore.sizeToFit()
        lblMore.textAlignment = .right
        lblMore.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        
        lblCatName.translatesAutoresizingMaskIntoConstraints = false
        lblMore.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(lblCatName)
        headerView.addSubview(lblMore)
        
        constraints.append(NSLayoutConstraint(item: lblCatName, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1.0, constant: 16.0))
        constraints.append(NSLayoutConstraint(item: lblCatName, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1.0, constant: 8.0))
        constraints.append(NSLayoutConstraint(item: lblCatName, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1.0, constant: -8.0))
        
        constraints.append(NSLayoutConstraint(item: lblMore, attribute: .trailing, relatedBy: .equal, toItem: headerView, attribute: .trailing, multiplier: 1.0, constant: -16.0))
        constraints.append(NSLayoutConstraint(item: lblMore, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1.0, constant: 8.0))
        constraints.append(NSLayoutConstraint(item: lblMore, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1.0, constant: -8.0))
        
        constraints.append(NSLayoutConstraint(item: lblCatName, attribute: .right, relatedBy: .greaterThanOrEqual, toItem: lblMore, attribute: .left, multiplier: 1.0, constant: 3.0))
        
        headerView.addConstraints(constraints)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let cat = self.categories[indexPath.section]
            self.recipes[cat]?.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            if let arr = self.recipes[cat], arr.count <= 0 {
                self.categories.remove(at: indexPath.section)
                self.tableView.reloadData()
                
            }
            
            var newRecipes = [[String : Any]]()
            for aCat in self.categories {
                if let recipes = self.recipes[aCat] {
                    for aRecipe in recipes {
                        let datas: [String : Any] = [
                            "id" : aRecipe.id ?? "",
                            "name" : aRecipe.title ?? "",
                            "imagePath" : aRecipe.imagePath ?? "",
                            "ingredients" : aRecipe.ingredients ?? "",
                            "steps" : aRecipe.steps ?? "",
                            "category" : ["catName" : aRecipe.category?.name]
                        ]
                        newRecipes.append(datas)
                    }
                    
                    UserDefaults.standard.set(newRecipes, forKey: "recipes")
                    UserDefaults.standard.synchronize()
                }
            }
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cat = self.categories[indexPath.section]
        if let recipes = self.recipes[cat] {
            self.selectedEditRecipe = recipes[indexPath.row]
            self.performSegue(withIdentifier: "showRecipeDetails", sender: self)
        }
    }
}
