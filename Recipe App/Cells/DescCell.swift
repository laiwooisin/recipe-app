//
//  DescCell.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import UIKit

protocol DescCellDelegate {
    func didTouchUpInside(indexPath: IndexPath)
}

class DescCell: UITableViewCell {
    
    public var indexPath: IndexPath!
    public var delegate: DescCellDelegate?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onTouchUpInside(sender: UIButton) {
        self.delegate?.didTouchUpInside(indexPath: self.indexPath)
    }

}
