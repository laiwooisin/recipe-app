//
//  TextFieldCell.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import UIKit

protocol TextFieldCellDelegate {
    func didFinishEditing(text: String)
    func onFocus(textField: UITextField)
}

class TextFieldCell: UITableViewCell {
    
    @IBOutlet weak var tfTitle: UITextField!
    
    public var delegate: TextFieldCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onBeginEditing(_ sender: Any) {
        if let tf = sender as? UITextField {
             self.delegate?.onFocus(textField: tf)
        }
    }
    
    @IBAction func onEndEditingText(_ sender: Any) {
        self.delegate?.didFinishEditing(text: self.tfTitle.text ?? "")
    }
}
