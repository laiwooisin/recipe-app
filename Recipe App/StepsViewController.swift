//
//  StepsViewController.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import UIKit

class StepsViewController: UIViewController {

    @IBOutlet weak var vText: UITextView!
    public var text: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let str = self.text {
            self.vText.text = str
        }
    }
    
    @IBAction func onDone(_ sender: UIButton) {
        self.performSegue(withIdentifier: "closeSteps", sender: self)
    }
    
    @IBAction func onTap(_ sender: UITapGestureRecognizer) {
        self.vText.resignFirstResponder()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
