//
//  NewRecipeViewController.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import UIKit
import DKImagePickerController

class NewRecipeViewController: UIViewController {
    
    private var catName: String = ""
    private var elementName: String = ""
    private var categories: [Category] = []
    private var foodImage: UIImage? {
        didSet {
            self.recipe.imagePath = self.foodImage?.jpegData(compressionQuality: 0.5)?.base64EncodedString()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewContainPicker: UIView!
    private var currentTextField: UITextField?
    
    public var recipe: Recipe!
    public var editingRecipe: Recipe? {
        didSet {
            if let toEditRecipe = self.editingRecipe {
                self.recipe = Recipe()
                self.recipe.id = toEditRecipe.id
                self.recipe = toEditRecipe
            }
        }
    }
    public var recipes: [[String : Any]]! = [[String : Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.recipe == nil {
            self.recipe = Recipe()
        }
        
        self.viewContainPicker.alpha = 0.0
        
        self.tableView.tableFooterView = UIView()
        
        if let recipes = UserDefaults.standard.object(forKey: "recipes") as? [[String : Any]] {
            self.recipes = recipes
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showIngredients":
            if let vc = segue.destination as? IngredientsViewController {
                vc.text = self.recipe.ingredients
            }
        case "showWriteSteps":
            if let vc = segue.destination as? StepsViewController {
                vc.text = self.recipe.steps
            }
        default:
            break
        }
    }
    
    @IBAction func onSelectedCategory(_ sender: UIButton) {
        self.setEditing(false, animated: true)
        
        let row = self.pickerView.selectedRow(inComponent: 0)
        let aCat = self.categories[row]
        self.recipe.category = aCat
        
        UIView.animate(withDuration: 0.3) {
            self.viewContainPicker.alpha = 0.0
        }
        
        self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        self.currentTextField?.resignFirstResponder()
        
        guard self.recipe.imagePath != nil else {
            self.showMessagePrompt(title: NSLocalizedString("Recipe App", comment: "App Name"),
                                   message: NSLocalizedString("Please provide the food image", comment: "Error message"))
            return
        }
        
        guard self.recipe.title != "" else {
            self.showMessagePrompt(title: NSLocalizedString("Recipe App", comment: "App Name"),
                                   message: NSLocalizedString("Title is empty", comment: "Error message"))
            return
        }
        
        guard self.recipe.ingredients != "" else {
            self.showMessagePrompt(title: NSLocalizedString("Recipe App", comment: "App Name"),
                                   message: NSLocalizedString("Ingredients is empty", comment: "Error message"))
            return
        }
        
        if let aditRecipe = self.editingRecipe {
            if self.recipe != aditRecipe {
                var ind = 0
                for rec in self.recipes {
                    if let recId = rec["id"] as? String, recId == aditRecipe.id {
                        self.recipes.remove(at: ind)
                        UserDefaults.standard.removePersistentDomain(forName: "recipes")
                        UserDefaults.standard.synchronize()
                        break
                    }
                    ind += 1
                }
            } else if self.recipe == aditRecipe {
                self.navigationController?.popViewController(animated: true)
                return
            }
        }
        
        let data: [String : Any] = [
            "id" : self.recipe.id ?? "",
            "name" : self.recipe.title ?? "",
            "imagePath" : self.recipe.imagePath ?? self.foodImage?.jpegData(compressionQuality: 0.5)?.base64EncodedString() ?? "" ,
            "ingredients" : self.recipe.ingredients ?? "",
            "steps" : self.recipe.steps ?? "",
            "category" : [
                "catName" : self.recipe.category?.name
            ]
        ]
        self.recipes.append(data)
        UserDefaults.standard.set(self.recipes, forKey: "recipes")
        UserDefaults.standard.synchronize()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func readXMLFile() {
        if let path = Bundle.main.url(forResource: "recipetypes", withExtension: "xml") {
            self.categories.removeAll()
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
    }
}

extension NewRecipeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        switch indexPath.row {
        case 0:
            cell = self.constructImageCell(indexPath)
        case 1:
            cell = self.constructTextFieldCell(indexPath)
        case 2,3,4:
            cell = self.constructDescCell(indexPath)
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentTextField?.resignFirstResponder()
    }
    
    private func constructImageCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
        cell.vImage?.image = nil
        cell.vImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector(("onPhoto:"))))
        
        if let image = self.foodImage {
            cell.vImage?.image = image
        } else {
            if let imgPath = self.recipe.imagePath, let data = Data(base64Encoded: imgPath) {
                let image = UIImage(data: data)
                cell.vImage.image = image
            }
        }
        
        return cell
    }
    
    private func constructTextFieldCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
        cell.delegate = self
        cell.tfTitle?.placeholder = ""
        cell.tfTitle?.placeholder = NSLocalizedString("Title", comment: "Textfield Placeholder title")
        
        cell.tfTitle.text = self.recipe.title
        return cell
    }
    
    private func constructDescCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "DescCell", for: indexPath) as! DescCell
        cell.lblTitle.text = ""
        cell.lblDesc.text = ""
        cell.indexPath = indexPath
        cell.delegate = self
        
        switch indexPath.row {
        case 2:
            cell.lblTitle.text = NSLocalizedString("Category", comment: "Label title")
            cell.lblDesc.text = self.recipe.category?.name
            cell.btn.setTitle(NSLocalizedString("Select", comment: "Button title name"), for: .normal)
        case 3:
            cell.lblTitle.text = NSLocalizedString("Ingredients", comment: "Label title")
            cell.lblDesc.text = self.recipe.ingredients
            cell.btn.setTitle(NSLocalizedString("Write", comment: "Button title name"), for: .normal)
        case 4:
            cell.lblTitle.text = NSLocalizedString("Steps", comment: "Label title")
            cell.lblDesc.text = self.recipe.steps
            cell.btn.setTitle(NSLocalizedString("Write", comment: "Button title name"), for: .normal)
        default:
            cell.lblTitle.text = ""
            cell.lblDesc.text = ""
        }
        return cell
    }
    
    @IBAction func onPhoto(_ sender: UITapGestureRecognizer) {
        self.selectPhotoOptions()
    }
    
    private func selectPhotoOptions() {
        let localizeMsg = NSLocalizedString("Select your photo from", comment: "An action sheet topic")
        
        let alertController = UIAlertController(title: nil, message: localizeMsg, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in }
        alertController.addAction(cancelAction)
        
        let PhotoGalleryAction = UIAlertAction(title: NSLocalizedString("Photo Library", comment: "An action name Photo Library"), style: .default) { action in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let pickerController = DKImagePickerController()
                pickerController.assetType = .allPhotos
                pickerController.maxSelectableCount = 1
                pickerController.showsCancelButton = true
                pickerController.navigationBar.isTranslucent = false
                pickerController.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.5920000076, blue: 0.8629999757, alpha: 1)
                pickerController.navigationBar.tintColor = UIColor.black
                self.present(pickerController, animated: true, completion: nil)
                
                pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
                    for asset in assets {
                        asset.fetchOriginalImageWithCompleteBlock({ [unowned self] (image, info) in
                            self.foodImage = image
                            self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                        })
                    }
                }
            }
        }
        alertController.addAction(PhotoGalleryAction)
        
        let CameraAction = UIAlertAction(title: NSLocalizedString("Camera", comment: "An action name Camera"), style: .default) { action in
            DKCamera.checkCameraPermission({ (isGranted) in
                if isGranted {
                    self.onCamera()
                } else {
                    self.showMessageToRequestGrantCameraAccessPermission()
                }
            })
        }
        alertController.addAction(CameraAction)
        self.present(alertController, animated: true) { }
    }
    
    private func onCamera() {
        let pickerController = DKImagePickerController()
        pickerController.sourceType = .camera
        self.present(pickerController, animated: true, completion: nil)
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            for asset in assets {
                asset.fetchOriginalImageWithCompleteBlock({ [unowned self] (image, info) in
                    self.foodImage = image
                    self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                })
            }
        }
    }
    
    private func showMessageToRequestGrantCameraAccessPermission() {
        let alertVc = UIAlertController(title: NSLocalizedString("IMPORTANT", comment: "Alert Title"),
                                        message: NSLocalizedString("Camera access permission required for capturing photos", comment: "Alert message"),
                                        preferredStyle: .alert)
        alertVc.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alertVc.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button title"),
                                        style: .cancel, handler: { (alert) -> Void in
                                            if let url = URL(string: UIApplication.openSettingsURLString) {
                                                if UIApplication.shared.canOpenURL(url) {
                                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                }
                                            }
        }))
        self.present(alertVc, animated: true, completion: nil)
    }
}

extension NewRecipeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let aCat = self.categories[row]
        return aCat.name
    }
}

extension NewRecipeViewController: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "recipetype" {
            self.catName = String()
        }
        self.elementName = elementName
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "recipetype" {
            let aCat = Category(name: self.catName)
            self.categories.append(aCat)
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if !data.isEmpty {
            if self.elementName == "title" {
                self.catName += data
            }
        }
    }
}

extension NewRecipeViewController {
    
    @IBAction func onDoneIngredients(segue: UIStoryboardSegue) {
        if segue.identifier == "closeIngredients" {
            if let vc = segue.source as? IngredientsViewController {
                if vc.vText.text != "" {
                    self.recipe.ingredients = vc.vText.text
                    self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
                }
            }
        }
    }
    
    @IBAction func onDoneSteps(segue: UIStoryboardSegue) {
        if segue.identifier == "closeSteps" {
            if let vc = segue.source as? StepsViewController {
                if vc.vText.text != "" {
                    self.recipe.steps = vc.vText.text
                    self.tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
                }
            }
        }
    }
}

extension NewRecipeViewController: DescCellDelegate, TextFieldCellDelegate {
    
    func didFinishEditing(text: String) {
        self.recipe.title = text
        self.currentTextField = .none
    }
    
    func onFocus(textField: UITextField) {
        self.currentTextField = textField
    }
    
    func didTouchUpInside(indexPath: IndexPath) {
        self.currentTextField?.resignFirstResponder()
        
        switch indexPath.row {
        case 2:
            UIView.animate(withDuration: 0.3) {
                self.viewContainPicker.alpha = 1.0
            }
            self.readXMLFile()
            self.pickerView.reloadAllComponents()
        case 3:
            self.performSegue(withIdentifier: "showIngredients", sender: self)
        case 4:
            self.performSegue(withIdentifier: "showWriteSteps", sender: self)
        default:
            break
        }
    }
}
