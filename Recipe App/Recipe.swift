//
//  Recipe.swift
//  Recipe App
//
//  Created by Lai Wooi Sin on 27/06/2019.
//  Copyright © 2019 Lai Wooi Sin. All rights reserved.
//

import Foundation

struct Category: Hashable {
    var name: String
}

struct Recipe: Hashable {
    var id: String!
    var title: String!
    var imagePath: String!
    var ingredients: String!
    var steps: String!
    var category: Category!
    
    init() {
        self.id = UUID().uuidString
    }
}
